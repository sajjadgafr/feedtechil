/* eslint-disable prefer-arrow-callback */
import TypesenseInstantSearchAdapter from 'typesense-instantsearch-adapter';
// eslint-disable-next-line import/no-extraneous-dependencies
import debounce from 'lodash.debounce';

const { algoliasearch, instantsearch } = window;

const typesenseInstantsearchAdapter = new TypesenseInstantSearchAdapter({
  server: {
    apiKey: '0PzTtspWDFhlMkteuxBRC297LsyC1Dxo',
    nodes: [
      {
        host: 'h3kz6fwxdqp7r5i4p-1.a1.typesense.net',
        port: '443',
        protocol: 'https',
      },
    ],
  },
  // The following parameters are directly passed to Typesense's search API endpoint.
  //  So you can pass any parameters supported by the search endpoint below.
  //  queryBy is required.
  additionalSearchParameters: {
    queryBy: 'tweet,tags,replies,userId,userName',
    queryByWeights: '4,3,2,1,1',
    // includeFields: 'tweet,userId,userName,userAvatar,createdAt,likes,comments,retweets',
    excludeFields: 'updatedAt',
    highlightFields: 'tweet,replies',
    numTypos: 1,
  },
});

const sendEventDebounced = debounce(() => {
  // window.ga('set', 'page', window.location.pathname + window.location.search);
  // window.ga('send', 'pageView');
  window.dataLayer = window.dataLayer || [];
  function gtag() {
    dataLayer.push(arguments);
  }
  gtag('js', new Date());
  gtag('config', 'G-VZSYM4S7PZ');
}, 3000);

const searchClient = typesenseInstantsearchAdapter.searchClient;

const search = instantsearch({
  searchClient,
  indexName: 'feedtechil',
  routing: {
    stateMapping: {
      stateToRoute(uiState) {
        const indexUiState = uiState.feedtechil;
        // eslint-disable-next-line no-shadow
        const searchQuery = indexUiState.query;
        document.title =
          searchQuery !== undefined
            ? `${searchQuery} | פיד טק`
            : 'חיפוש בפיד טק';
        return {
          q: searchQuery,
          p: indexUiState.page,
        };
      },
      routeToState(routeState) {
        return {
          feedtechil: {
            query: routeState.q,
            // page: routeState.p,
          },
        };
      },
    },
  },
});

search.use(() => ({
  onStateChange({ uiState }) {
    sendEventDebounced(uiState);
  },
}));

search.addWidgets([
  instantsearch.widgets.searchBox({
    container: '#searchbox',
    placeholder: 'חיפוש בפיד טק',
  }),

  instantsearch.widgets.hits({
    container: '#topSection',
    templates: {
      item: `
       <img src="{{ userAvatar }}" class="border border-light rounded-circle hit-avatar-img" title="{{ uniqueName }}">
       <span class="grey-color d-block">{{ uniqueUsername }}@</span>
      <a href="{{ twitterProfile }}" target="_blank" class="stretched-link d-md-none"></a>
      <a href="{{ twitterProfile }}" class="stretched-link  d-none d-md-block"></a>
      `,
      empty: '',
    },

    transformItems(items) {
      const key = 'userId';
      const users = findOcc(items, key);
      return users.map(item => ({
        ...item,
        uniqueUsername: (() => item.userId)(),
        uniqueName: (() => item.userName)(),
        userAvatar: (() => item.userAvatar)(),
        twitterProfile: (() => `https://twitter.com/${item.userId}`)(),
      }));
    },
  }),
  instantsearch.widgets.infiniteHits({
    container: '#hits',
    escapeHTML: true,
    templates: {
      item: `
        <div>
        <div class="d-flex main-tweet">
         <div class="hit-avatar-col pe-ms-3 pe-2 text-center">
           <a href="{{ tweetProfileLink }}" target="_blank"  class="z-index position-relative d-inline-block d-md-none"><img src="{{userAvatar}}" align="left" class="border border-light rounded-circle hit-avatar-img" alt="{{userId}}" title="{{userName}}" /></a>
           <a href="{{ tweetProfileLink }}" class="z-index position-relative d-inline-block d-none d-md-block"><img src="{{userAvatar}}" align="left" class="border border-light rounded-circle hit-avatar-img" alt="{{userId}}" title="{{userName}}" /></a>
            <div class="d-inline-block d-md-none pt-2 pt-md-0 ps-2 tweet-action-right">
                <div class="hit-comments mb-2 pb-1 grey-color">
                <a href="#" class="d-flex align-items-center z-index position-relative ">
                <div class="mobile-action-icon me-1"><svg viewBox="0 0 24 24" aria-hidden="true" class="r-4qtqp9 r-yyyyoo r-1xvli5t r-dnmrzs r-bnwqim r-1plcrui r-lrvibr r-1hdv0qi"><g><path d="M14.046 2.242l-4.148-.01h-.002c-4.374 0-7.8 3.427-7.8 7.802 0 4.098 3.186 7.206 7.465 7.37v3.828c0 .108.044.286.12.403.142.225.384.347.632.347.138 0 .277-.038.402-.118.264-.168 6.473-4.14 8.088-5.506 1.902-1.61 3.04-3.97 3.043-6.312v-.017c-.006-4.367-3.43-7.787-7.8-7.788zm3.787 12.972c-1.134.96-4.862 3.405-6.772 4.643V16.67c0-.414-.335-.75-.75-.75h-.396c-3.66 0-6.318-2.476-6.318-5.886 0-3.534 2.768-6.302 6.3-6.302l4.147.01h.002c3.532 0 6.3 2.766 6.302 6.296-.003 1.91-.942 3.844-2.514 5.176z"></path></g></svg></div>{{comments}}
                  </a>
                </div>
                <div class="hit-retweets mb-2 pb-1 grey-color">
                   <a href="#" class="d-flex align-items-center z-index position-relative ">
                   <div class="mobile-action-icon me-1"><svg viewBox="0 0 24 24" aria-hidden="true" class="r-4qtqp9 r-yyyyoo r-1xvli5t r-dnmrzs r-bnwqim r-1plcrui r-lrvibr r-1hdv0qi"><g><path d="M23.77 15.67c-.292-.293-.767-.293-1.06 0l-2.22 2.22V7.65c0-2.068-1.683-3.75-3.75-3.75h-5.85c-.414 0-.75.336-.75.75s.336.75.75.75h5.85c1.24 0 2.25 1.01 2.25 2.25v10.24l-2.22-2.22c-.293-.293-.768-.293-1.06 0s-.294.768 0 1.06l3.5 3.5c.145.147.337.22.53.22s.383-.072.53-.22l3.5-3.5c.294-.292.294-.767 0-1.06zm-10.66 3.28H7.26c-1.24 0-2.25-1.01-2.25-2.25V6.46l2.22 2.22c.148.147.34.22.532.22s.384-.073.53-.22c.293-.293.293-.768 0-1.06l-3.5-3.5c-.293-.294-.768-.294-1.06 0l-3.5 3.5c-.294.292-.294.767 0 1.06s.767.293 1.06 0l2.22-2.22V16.7c0 2.068 1.683 3.75 3.75 3.75h5.85c.414 0 .75-.336.75-.75s-.337-.75-.75-.75z"></path></g></svg></div>{{retweets}}</a>
                </div>
                 <div class="hit-likes grey-color">
                <a href="#" class="d-flex align-items-center z-index position-relative ">
                <div class="mobile-action-icon me-1"><svg viewBox="0 0 24 24" aria-hidden="true" class="r-4qtqp9 r-yyyyoo r-1xvli5t r-dnmrzs r-bnwqim r-1plcrui r-lrvibr r-1hdv0qi"><g><path d="M12 21.638h-.014C9.403 21.59 1.95 14.856 1.95 8.478c0-3.064 2.525-5.754 5.403-5.754 2.29 0 3.83 1.58 4.646 2.73.814-1.148 2.354-2.73 4.645-2.73 2.88 0 5.404 2.69 5.404 5.755 0 6.376-7.454 13.11-10.037 13.157H12zM7.354 4.225c-2.08 0-3.903 1.988-3.903 4.255 0 5.74 7.034 11.596 8.55 11.658 1.518-.062 8.55-5.917 8.55-11.658 0-2.267-1.823-4.255-3.903-4.255-2.528 0-3.94 2.936-3.952 2.965-.23.562-1.156.562-1.387 0-.014-.03-1.425-2.965-3.954-2.965z"></path></g></svg></div>{{likes}}</a>
                </div>
              </div>
          </div>

          <div class="flex-grow-1 tweet-content">
            <div class="d-flex justify-content-between tweet-actions mb-1">
              <div class="d-flex pe-md-4 tweet-action-left">
                <div class="fw-bold hit-userName me-1">
                  <a href="{{tweetProfileLink}}" target="_blank" class="z-index position-relative text-nowrap stretched-link d-md-none">{{userName}}</a>
                  <a href="{{tweetProfileLink}}" class="z-index position-relative text-nowrap stretched-link d-none d-md-block">{{userName}}</a>
                </div>
                <div class="hit-userId me-1 d-none d-md-block grey-color">
                  <a href="{{tweetProfileLink}}" target="_blank" class="z-index position-relative text-nowrap stretched-link d-md-none">{{userId}}@</a>
                  <a href="{{tweetProfileLink}}" class="z-index position-relative text-nowrap stretched-link d-none d-md-block">{{userId}}@</a>
                </div>
                <div class="hit-userId me-1 grey-color">
                  -
                </div>
                <div class="hit-created_at text-nowrap grey-color">
                  <a href="#" class="text-nowrap align-items-center d-flex" title="{{ createdDateTooltip }}">
                  {{createdDateDisplay}}
                  </a>
                </div>
              </div>

              <div class="d-flex tweet-action-right d-none d-md-flex">
                <div class="hit-likes grey-color me-xl-4 me-md-4 me-3 pe-1">
                <a href="#" class="text-nowrap align-items-center d-flex">
                  <svg class="me-2" viewBox="0 0 24 24" aria-hidden="true" class="r-4qtqp9 r-yyyyoo r-1xvli5t r-dnmrzs r-bnwqim r-1plcrui r-lrvibr r-1hdv0qi"><g><path d="M12 21.638h-.014C9.403 21.59 1.95 14.856 1.95 8.478c0-3.064 2.525-5.754 5.403-5.754 2.29 0 3.83 1.58 4.646 2.73.814-1.148 2.354-2.73 4.645-2.73 2.88 0 5.404 2.69 5.404 5.755 0 6.376-7.454 13.11-10.037 13.157H12zM7.354 4.225c-2.08 0-3.903 1.988-3.903 4.255 0 5.74 7.034 11.596 8.55 11.658 1.518-.062 8.55-5.917 8.55-11.658 0-2.267-1.823-4.255-3.903-4.255-2.528 0-3.94 2.936-3.952 2.965-.23.562-1.156.562-1.387 0-.014-.03-1.425-2.965-3.954-2.965z"></path></g></svg>
                  {{likes}}</a>
                </div>
                <div class="hit-comments me-xl-4 me-md-4 me-3 pe-1 grey-color">
                <a href="#" class="text-nowrap align-items-center d-flex">
                <svg class="me-2" viewBox="0 0 24 24" aria-hidden="true" class="r-4qtqp9 r-yyyyoo r-1xvli5t r-dnmrzs r-bnwqim r-1plcrui r-lrvibr r-1hdv0qi"><g><path d="M14.046 2.242l-4.148-.01h-.002c-4.374 0-7.8 3.427-7.8 7.802 0 4.098 3.186 7.206 7.465 7.37v3.828c0 .108.044.286.12.403.142.225.384.347.632.347.138 0 .277-.038.402-.118.264-.168 6.473-4.14 8.088-5.506 1.902-1.61 3.04-3.97 3.043-6.312v-.017c-.006-4.367-3.43-7.787-7.8-7.788zm3.787 12.972c-1.134.96-4.862 3.405-6.772 4.643V16.67c0-.414-.335-.75-.75-.75h-.396c-3.66 0-6.318-2.476-6.318-5.886 0-3.534 2.768-6.302 6.3-6.302l4.147.01h.002c3.532 0 6.3 2.766 6.302 6.296-.003 1.91-.942 3.844-2.514 5.176z"></path></g></svg>
                   {{comments}}
                  </a>
                </div>
                <div class="hit-retweets grey-color">
                   <a href="#" class="text-nowrap align-items-center d-flex">
                   <svg class="me-2" viewBox="0 0 24 24" aria-hidden="true" class="r-4qtqp9 r-yyyyoo r-1xvli5t r-dnmrzs r-bnwqim r-1plcrui r-lrvibr r-1hdv0qi"><g><path d="M23.77 15.67c-.292-.293-.767-.293-1.06 0l-2.22 2.22V7.65c0-2.068-1.683-3.75-3.75-3.75h-5.85c-.414 0-.75.336-.75.75s.336.75.75.75h5.85c1.24 0 2.25 1.01 2.25 2.25v10.24l-2.22-2.22c-.293-.293-.768-.293-1.06 0s-.294.768 0 1.06l3.5 3.5c.145.147.337.22.53.22s.383-.072.53-.22l3.5-3.5c.294-.292.294-.767 0-1.06zm-10.66 3.28H7.26c-1.24 0-2.25-1.01-2.25-2.25V6.46l2.22 2.22c.148.147.34.22.532.22s.384-.073.53-.22c.293-.293.293-.768 0-1.06l-3.5-3.5c-.293-.294-.768-.294-1.06 0l-3.5 3.5c-.294.292-.294.767 0 1.06s.767.293 1.06 0l2.22-2.22V16.7c0 2.068 1.683 3.75 3.75 3.75h5.85c.414 0 .75-.336.75-.75s-.337-.75-.75-.75z"></path></g></svg>
                    {{retweets}}</a>
                </div>

              </div>
            </div>
            <div class="tweet-description">
              <div class="hit-name">
                {{tweetDetail}}
              </div>
            </div>
            <a href="{{tweetLink}}" target="_blank" class="stretched-link d-md-none"></a>
            <a href="{{tweetLink}}" class="stretched-link d-none d-md-block"></a>
          </div>
           
        </div>
        <div class="reply-tweet d-flex mt-md-3 mt-4 pt-md-0">
              <div class="hit-avatar-col pe-ms-3 pe-2 text-center">
              <a href="{{ tweetProfileLink }}" target="_blank"  class="z-index position-relative d-inline-block d-md-none"><img src="{{userAvatar}}" align="left" class="border border-light rounded-circle hit-avatar-img" alt="{{userId}}" title="{{userName}}" /></a>
              <a href="{{ tweetProfileLink }}" class="z-index position-relative d-inline-block d-none d-md-block"><img src="{{userAvatar}}" align="left" class="border border-light rounded-circle hit-avatar-img" alt="{{userId}}" title="{{userName}}" /></a>
              </div>
              <div class="flex-grow-1 tweet-content tweet-description">
                  <div class="hit-name">
                    {{tweetDetail}}
                  </div>
              </div>
          </div>
        </div>
      `,
      showMoreText: 'עוד תוצאות',
      empty: 'לא נמצאו תוצאות',
    },
    transformItems: items =>
      // transformItems(items) {
      items.map(item => ({
        ...item,
        tweetDetail: (() => {
          let tw = item.tweet;
          tw = tw.replaceAll('&amp;', '&');
          tw = tw.replaceAll('&lt;', '<');
          tw = tw.replaceAll('&gt;', '>');
          tw = tw.replaceAll('&#39', "'");
          tw = tw.replaceAll('&quot;', '"');
          return tw;
        })(),
        createdDateDisplay: (() => {
          const current = new Date().getTime();
          const previous = item.createdAt;

          const msPer1Minute = 1 * 1000;
          const msPerMinute = 60 * 1000;
          const msPer100Minute = 100 * 1000;
          const msPer140Minute = 140 * 1000;
          const msPerHour = msPerMinute * 60;
          const msPerDay = msPerHour * 24;
          const msPer24Hours = msPerHour * 24;
          const msPer25Hours = msPerHour * 25;
          const msPer48Hours = msPerHour * 48;
          const msPerMonth = msPerDay * 30;
          const msPer7days = msPerDay * 7;
          const msPer8days = msPerDay * 8;
          const msPer14days = msPerDay * 14;
          const msPer15days = msPerDay * 15;
          const msPerweeks = msPerDay * 7;
          const msPer5weeks = msPerDay * 35;
          const msPer2Months = msPerMonth * 2;
          const msPer3Months = msPerMonth * 3;
          const msPer12Months = msPerMonth * 12;

          const elapsed = current - previous;
          if (elapsed <= 60 * msPer1Minute) {
            return 'לפני דקה';
          } else if (elapsed < 60 * msPer140Minute) {
            return 'לפני שעתיים';
          } else if (elapsed < 60 * msPer100Minute) {
            return 'לפני שעה';
          } else if (elapsed < msPer24Hours) {
            return `לפני ${Math.round(elapsed / msPerHour)} שעות`;
          } else if (elapsed < msPer25Hours) {
            return 'אתמול';
          } else if (elapsed < msPer48Hours) {
            return 'שלשום';
          } else if (elapsed < msPer7days) {
            return `לפני ${Math.round(elapsed / msPerDay)} ימים`;
          } else if (elapsed < msPer8days) {
            return 'לפני שבוע';
          } else if (elapsed < msPer14days) {
            return `לפני ${Math.round(elapsed / msPerDay)} ימים`;
          } else if (elapsed < msPer15days) {
            return 'לפני שבועיים';
          } else if (elapsed < msPer5weeks) {
            return `לפני ${Math.round(elapsed / msPerweeks)} שבועות`;
          } else if (elapsed < msPer3Months) {
            return 'לפני חודשיים';
          } else if (elapsed < msPer2Months) {
            return 'לפני חודש';
          } else if (elapsed < msPer12Months) {
            return `לפני  ${Math.round(elapsed / msPerMonth)} חודשים`;
          } else {
            const date = new Date(item.createdAt);
            const day = date.getDate();
            const month = date.getMonth() + 1;
            const year = date.getFullYear();
            const hebrewMonth = returnMonthNameInHebrew(month);
            return `${day} ב${hebrewMonth} ${year}`;
          }
        })(),
        tweetLink: (() =>
          `https://twitter.com/${item.userId}/status/${item.id}`)(),
        tweetProfileLink: (() => `https://twitter.com/${item.userId}`)(),
        userAvatar: (() => item.userAvatar)(),
        createdDateTooltip: (() => {
          const date = new Date(item.createdAt);
          const day = date.getDate();
          const month = date.getMonth() + 1;
          const year = date.getFullYear();
          const hebrewMonth = returnMonthNameInHebrew(month);
          return `${day} ב${hebrewMonth} ${year}`;
        })(),
      })),
  }),
]);

function returnMonthNameInHebrew(month) {
  return {
    1: 'ינואר',
    2: 'פברואר',
    3: 'מרץ',
    4: 'אפריל',
    5: 'מאי',
    6: 'יוני',
    7: 'יולי',
    8: 'אוגוסט',
    9: 'ספטמבר',
    10: 'אוקטובר',
    11: 'נובמבר',
    12: 'דצמבר',
  }[month];
}

function findOcc(arr, key) {
  const arr2 = [];
  arr.forEach(item => {
    // Checking if there is any object in arr2
    // which contains the key value
    if (arr2.some(val => val[key] === item[key])) {
      // If yes! then increase the occurrence by 1
      arr2.forEach(k => {
        if (k[key] === item[key]) {
          // eslint-disable-next-line no-param-reassign
          k.occurrence++;
        }
      });
    } else {
      // If not! Then create a new object initialize
      // it with the present iteration key's value and
      // set the occurrence to 1
      const a = {};
      a[key] = item[key];
      a.occurrence = 1;
      a.userAvatar = item.userAvatar;
      a.userName = item.userName;
      arr2.push(a);
    }
  });
  // sort by number of occurrence
  arr2.sort((a, b) => b.occurrence - a.occurrence);
  return arr2;
}

search.start();
